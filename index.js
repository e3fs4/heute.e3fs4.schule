import dotenv from 'dotenv';
import express from 'express';
import { WebUntis } from 'webuntis';
import * as path from 'path';

// This is important for the .env Configuration file!
dotenv.config();

const __dirname = path.resolve();
const ip = process.env.IP;
const port = process.env.PORT;
const rIntervall = process.env.REFRESH_INTERVALL;

let data = [];
let timetable = [];

async function getWebUntisToday(date1, date2) {
	const schoolname = process.env.SCHOOL_NAME;
	const username = process.env.USERNAME;
	const password = process.env.PASSWORD;
	const link = process.env.SCHOOL_URL;
	const untis = new WebUntis(`${schoolname}`, `${username}`, `${password}`, `${link}`);
	await untis.login();
    console.log("getWebUntisToday");
    console.log(untis.getOwnTimetableForRange(date1, date2));
	return await untis.getOwnTimetableForRange(date1, date2);
}

function getDate(){
    // TODO: Delete Date to get the date for today
    console.log("getDate");
    console.log(new Date());
    return new Date();
}

function getTime(){
    let letTimeToday = new Date(),
    hh = letTimeToday.getHours() + 2,
    min = String(letTimeToday.getMinutes()).padStart(2, "0");
    //TODO: replace "1200" with hh + min
    console.log("getTime");
    console.log(hh+min);
    return hh + min;
}

async function getNow() {
    let timeToday = getTime();

    // So kriegt man ein Subject raus, da wird auch die Zeit angezeigt
    /* timetable.forEach(function(subject) {
        if (subject.su[0].name == "E") {
            console.log(subject.startTime);
            console.log(subject.endTime);
        }
    });*/

    console.log("getNow timetable.length");
    console.log(timetable.length);
    console.log("getNow timetable");
    console.log(timetable);
    if (timetable.length != 0) {
        let dataNow = [];
        timetable.forEach(function(lesson) {
            if (Number(timeToday) >= lesson.startTime && Number(timeToday) <= lesson.endTime) {
                dataNow.push(lesson.startTime);
                dataNow.push(lesson.endTime);
                if (lesson.su.length != 0) {
                    dataNow.push(lesson.su[0].name);
                } else if (lesson.substText != "" || lesson.substText != null) {
                    dataNow.push(lesson.substText);
                } else if (lesson.lstext != "" || lesson.lstext != null) {
                    dataNow.push(lesson.lstext);
                } else {
                    dataNow.push("Gibbet nicht!");
                }
                if (lesson.code != null) {
                    switch (lesson.code) {
                        case "cancelled":
                            dataNow.push("Entfällt");
                            break;
                        case "irregular":
                            dataNow.push("Verschoben");
                            break;
                    }
                } else if (lesson.substText != null) {
                    dataNow.push("Vertretung");
                    dataNow.push(lesson.substText);
                }
                console.log("getNow Works")
                console.log(dataNow)
            }
        });
        data[0] = dataNow;
    } else {

        data[0] = [];
    }
}

// TODO: Add breaks
async function getBreaks() {

}

// TODO: Add Holidays
async function getHolidays() {

}

// TODO: Add Weekends
async function getWeekend() {

}

async function getToday() {
    let timeToday = getTime();
    // TODO: Change timetable from For to Today
    //const timetable = await untis.getOwnTimetableForToday();
    
    console.log("getToday timetable.length");
    console.log(timetable.length);
    if (timetable.length != 0) {

        timetable.forEach(function(lesson) {
            let i = [];
            if (Number(timeToday) < lesson.startTime) {
                i.push(lesson.startTime);
                i.push(lesson.endTime);
                i.push(lesson.su[0].name);
                if (lesson.code != null) {
                    switch (lesson.code) {
                        case "cancelled":
                            i.push("Entfällt");
                            break;
                        case "irregular":
                            i.push("Verschoben");
                            break;
                    }
                } else if (lesson.substText != null) {
                    i.push("Vertretung");
                    i.push(lesson.substText);
                } else {
                    i.push(lesson.activityType);
                }
                console.log("getToday Works");
                console.log(i);
                data.push(i);
            }
        });
    } else {
        data[1] = [];
    }
}

//TODO: Getting next school day to display if there is no school
async function getNextSchoolDay(){
    let timetableForNextTime

    if (timetable.length == 0){
        if(getDate() <= new Date("2023-04-29")){
            timetableForNextTime = await getWebUntisToday(getDate(), new Date("2023-04-29"));
            // Sort the Array by startTime to get the correct order
            timetableForNextTime.sort((a,b) => a.date - b.date);
            console.log("getNextSchoolDay Works");
            console.log(timetableForNextTime[0].date);
            data.push(timetableForNextTime[0].date);
        } else {
            data.push("done")
        }
    }

}


async function interval() {
    await main()
}
setInterval(interval, rIntervall);

async function main() {
    data = [];
    timetable = await getWebUntisToday(getDate(), getDate())
    // Sort the Array by startTime to get the correct order
    timetable.sort((a,b) => a.startTime - b.startTime);
    await getNow();
    await getToday();
    await getNextSchoolDay();
}

const app = express();

app.use(express.static(path.join(__dirname, '/src')));

app.get('/', async function(req, res) {
    res.sendFile(path.join(__dirname, '/src/main.html'));
});

app.get('/data', async function (req, res) {
	res.json(data);
});

app.listen(port, ip);
console.log(`Server running at ${ip}:${port}`);