const nothing = document.querySelector('#nothing');
const lessonNothing = document.querySelector('#lessonNothing');
const startTime = document.querySelector('#startTime');
const endTime = document.querySelector('#endTime');
const subName = document.querySelector('#name');
const subType = document.querySelector('#type');
const irregularStartTime = document.querySelector('#irregularStartTime');
const irregularEndTime = document.querySelector('#irregularEndTime');
const irregularName = document.querySelector('#irregularName');
const comingUpWrapper = document.querySelector('#lessonToday');

function getCorrectLessonName(subject) {
    // Translate subject to right Name
    switch (subject) {
        case "BfK-B":
            return "BWL";
        case "BfK-I":
            return "ITS";
        case "BfK-S":
            return "SAE";
        case "D":
            return "Deutsch";
        case "E":
            return "Englisch";
        case "GK":
            return "Gemeinschaftskunde";
        case "WI":
            return "Wirtschaft";
    }
}

// Converts the given time
function convertTime(time) {
    let tempDate = null;
    tempDate = new Date(0);
    tempDate.setHours(time / 100);
    tempDate.setMinutes(time % 100);
    return tempDate.toLocaleTimeString('de-DE', { hour: '2-digit', minute: '2-digit' });
}

//http://127.0.0.1:38300/data
fetch('https://heute.fi.wern3r.de/data')
    .then(response => response.json())
    .then(async data => {
        let date = new Date();
        hh = date.getHours() + 1;
        min = String(date.getMinutes()).padStart(2, "0");

        if (data[0].length != 0) {
            //if ((hh + min > "930" && hh + min < "950") || (hh + min > "1120" && hh + min < "1130") || (hh + min > "1300" && hh + min < "1400")) { 
                document.getElementById('headlineIrregular').style.display = 'none';
                switch (data[0][0]){
                    case 845:
                        startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(800)} Uhr`);
                        break;
                    case 1035:
                        startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(950)} Uhr`);
                        break;
                    case 1215:
                        startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(1130)} Uhr`);
                        break;
                    case 1355:
                        startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(1345)} Uhr`);
                        break;
                    case 1450:
                        startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(1345)} Uhr`);
                        break;
                    default:
                        startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(data[0][0])} Uhr`);
                        break;
                }
                
                let end;
                if(Array.isArray(data[1]) && data[0][2] == data[1][2]){
                    switch (data[0][0]) {
                        case 800:
                            end = data[1][1];
                            endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[1][1])} Uhr`);
                            break;
                        case 950:
                            end = data[1][1];
                            endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[1][1])} Uhr`);
                            break;
                        case 1130:
                            end = data[1][1];end = data[1][1];
                            endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[1][1])} Uhr`);
                            break;
                        case 1355:
                            end = "1525";
                            endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime("1515")} Uhr`);
                            break;
                        default:
                            end = data[0][1]
                            endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[0][1])} Uhr`);  
                    }
                } else {
                    end = data[0][1]
                    endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[0][1])} Uhr`);
                }
                subName.insertAdjacentHTML("beforeend", `${getCorrectLessonName(data[0][2])}`);
                subType.insertAdjacentHTML("beforeend", "Diese Stunde ist regulärer Unterricht.");
                const second = 1000,
                    minute = second * 60,
                    hour = minute * 60,
                    day = hour * 24;

                let dateandtimetoday = new Date(),
                    dd = String(dateandtimetoday.getDate()).padStart(2, "0"),
                    mm = String(dateandtimetoday.getMonth() + 1).padStart(2, "0"),
                    yyyy = dateandtimetoday.getFullYear();

                enddate = yyyy + "-" + mm + "-" + dd + "T" + convertTime(end) + ":00";
                const countDown = new Date(enddate).getTime(),
                    x = setInterval(function () {

                        const now = new Date().getTime(),
                            distance = countDown - now;

                        document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                            document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                            document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                        //do something later when date is reached
                        if (distance < 0) {
                            document.getElementById('headlineCurrent').innerHTML = "Stunde beendet";
                            document.getElementById('headlineIrregular').style.display = 'none';
                            document.getElementById('headlineIrregular').innerHTML = `<img src="./img/over.gif">`;
                            subType.style.display = "none";
                            startTime.style.display = "none";
                            endTime.style.display = "none";
                            document.getElementById("countdown").style.display = "none";
                            clearInterval(x);
                        }
                        //seconds
                    }, 0)
                switch (data[0][3]) {
                    case "Entfällt":
                        subType.insertAdjacentHTML("beforeend", "Diese Stunde entfällt!");
                        document.getElementById('headlineIrregular').style.display = 'none';
                        break;
                    case "Verschoben":
                        subType.insertAdjacentHTML("beforeend", "Diese Stunde wurde verschoben!");
                        irregularName.insertAdjacentHTML("beforeend", `Fach: ${getCorrectLessonName(data[0][6])}`);
                        irregularStartTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(data[0][4])} Uhr`);
                        irregularEndTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[0][5])} Uhr`);
                        switch (data[1][7]) {
                            case "Entfällt":
                                irregularType.insertAdjacentHTML("beforeend", "Diese Stunde entfällt!");
                                break;
                            case "Verschoben":
                                irregularType.insertAdjacentHTML("beforeend", "Diese Stunde wurde verschoben.");
                                break;
                        }
                }
            /* } else {
                changeBackgroundColor();
                document.getElementById('headlineCurrent').innerHTML = "Es ist Pause!";
                document.getElementById('headlineIrregular').style.display = 'none';
                document.getElementById('countdown').style.display = 'none';
            } */
        } else if(data[0].length == 0 && Array.isArray(data[1]) && data[1][0] == 1450){
            document.getElementById('headlineIrregular').style.display = 'none';
            startTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(1345)} Uhr`);
            end = data[1][1];
            endTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[1][1])} Uhr`);
            subName.insertAdjacentHTML("beforeend", `${getCorrectLessonName(data[1][2])}`);
                subType.insertAdjacentHTML("beforeend", "Diese Stunde ist regulärer Unterricht.");
                const second = 1000,
                    minute = second * 60,
                    hour = minute * 60,
                    day = hour * 24;

                let dateandtimetoday = new Date(),
                    dd = String(dateandtimetoday.getDate()).padStart(2, "0"),
                    mm = String(dateandtimetoday.getMonth() + 1).padStart(2, "0"),
                    yyyy = dateandtimetoday.getFullYear();

                enddate = yyyy + "-" + mm + "-" + dd + "T" + convertTime(end) + ":00";
                const countDown = new Date(enddate).getTime(),
                    x = setInterval(function () {

                        const now = new Date().getTime(),
                            distance = countDown - now;

                        document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                            document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                            document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                        //do something later when date is reached
                        if (distance < 0) {
                            document.getElementById('headlineCurrent').innerHTML = "Stunde beendet";
                            document.getElementById('headlineIrregular').style.display = 'none';
                            document.getElementById('headlineIrregular').innerHTML = `<img src="./img/over.gif">`;
                            subType.style.display = "none";
                            startTime.style.display = "none";
                            endTime.style.display = "none";
                            document.getElementById("countdown").style.display = "none";
                            clearInterval(x);
                        }
                        //seconds
                    }, 0)
                switch (data[1][3]) {
                    case "Entfällt":
                        subType.insertAdjacentHTML("beforeend", "Diese Stunde entfällt!");
                        document.getElementById('headlineIrregular').style.display = 'none';
                        break;
                    case "Verschoben":
                        subType.insertAdjacentHTML("beforeend", "Diese Stunde wurde verschoben!");
                        irregularName.insertAdjacentHTML("beforeend", `Fach: ${getCorrectLessonName(data[1][6])}`);
                        irregularStartTime.insertAdjacentHTML("beforeend", `Beginn: ${convertTime(data[1][4])} Uhr`);
                        irregularEndTime.insertAdjacentHTML("beforeend", `Ende: ${convertTime(data[1][5])} Uhr`);
                        switch (data[1][7]) {
                            case "Entfällt":
                                irregularType.insertAdjacentHTML("beforeend", "Diese Stunde entfällt!");
                                break;
                            case "Verschoben":
                                irregularType.insertAdjacentHTML("beforeend", "Diese Stunde wurde verschoben.");
                                break;
                        }
                }
            /* } else {
                document.getElementById('headlineCurrent').innerHTML = "Es ist Pause!";
                document.getElementById('headlineIrregular').style.display = 'none';
                document.getElementById('countdown').style.display = 'none';
            } */
        } else {
            if (data[data.length - 1] != 0 && data[data.length -1] != "done" && data[data.length-1].length != 4){
                let day = String(data[data.length - 1])[6] + String(data[data.length - 1])[7]
                let month = String(data[data.length - 1])[4] + String(data[data.length - 1])[5]
                let year = String(data[data.length - 1])[0] + String(data[data.length - 1])[1] + String(data[data.length - 1])[2] + String(data[data.length - 1])[3]
                document.getElementById('headlineCurrent').innerHTML = "Aktuell keine Schule!";
                subType.insertAdjacentHTML("beforeend", "Nächste Stunde wieder am " + day + "." + month + "." + year);
                document.getElementById('headlineIrregular').style.display = 'none';
                document.getElementById('dash').style.display = 'none';
                document.getElementById('countdown').style.display = 'none';
                document.getElementById('tableNow').style.width = '100%';
                document.getElementById('tableToday').style.display = 'none';
            } else if(data[data.length -1] == "done") {
                document.getElementById('headlineCurrent').innerHTML = "Fertig mit der Schule!!!";
                document.getElementById('headlineIrregular').style.display = 'none';
                document.getElementById('countdown').style.display = 'none';
                document.getElementById('dash').style.display = 'none';
                document.getElementById('tableNow').style.width = '100%';
                document.getElementById('tableToday').style.display = 'none';
            } else {
                document.getElementById('headlineCurrent').innerHTML = "Aktuell keine Schule!";
                document.getElementById('headlineIrregular').style.display = 'none';
                document.getElementById('countdown').style.display = 'none';
                document.getElementById('dash').style.display = 'none';
                document.getElementById('tableNow').style.width = '100%';
                document.getElementById('tableToday').style.display = 'none';
            }
        }

        if(data[1].length != 0){
            let hoursToday = [];
            for (let i = 1; i < data.length; i++) {

                if (data[0].length == 0) {

                }else if (data[i-1][2] != data[i][2]) {
                    if(data[i][2] == data[i+1][2]){
                        comingUpWrapper.insertAdjacentHTML('beforeend', '<p>' + getCorrectLessonName(data[i][2]) + '<span>Beginn: ' + convertTime(data[i][0]) + ' - Ende: ' + convertTime(data[i+1][1]) + '</span>' + '</p>')
                    }
                }
            }
        }
    })
    .catch(error => {
        console.log("Error fetching data:", error);
    });